var express = require('express')
var serveStatic = require('serve-static')
var path = require('path')
var nodemailer = require('nodemailer')
var bodyParser = require('body-parser')
var fs = require('fs');
var json2csv = require('json2csv').parse;

var GMAILSECRETS = require('./secrets.js')
 
var app = express()

app.use(bodyParser.json());

app.post('/upload', (req, res) => {
	//save req.body as csv
	const fields = ['participantId', 'ratings', 'songChoice','imageChoice'];
	const opts = { fields };

	console.log(JSON.stringify(req.body))
	 
	try {
		  const csv = json2csv(req.body, opts);
		  console.log(csv);

		fs.appendFile('data.csv', csv, function (err) {
			  if (err) throw err;
			  console.log('Saved!');
		});
	} catch (err) {
		  console.error(err);
	}
})

 
app.use(serveStatic(path.join(__dirname, '/build')))

app.listen(80, () => {
  console.log("listening...")
})
