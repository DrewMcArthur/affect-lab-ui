import React, { Component } from 'react';
import ReactPlayer from 'react-player';
import Duration from './Duration.js';
import { DEBUG } from './App.js'

class MusicPlayer extends Component {
  state = {
    url: this.props.song,
    playing: false,
    volume: 0.8,
    muted: false,
    played: 0,
    loaded: 0,
    duration: 0,
    playbackRate: 1.0,
    loop: false,
    hidden: false
  }

  /**
   * functionality / controls
   */
  
  load = url => {
    this.setState({
      url,
      played: 0,
      loaded: 0
    })
  }

  playPause = () => {
    this.setState({ playing: !this.state.playing })
  }
  
  stop = () => {
    this.setState({ url: null, playing: false })
  }

  toggleLoop = () => {
    this.setState({ loop: !this.state.loop })
  }

  setVolume = e => {
    this.setState({ volume: parseFloat(e.target.value) })
  }

  toggleMuted = () => {
    this.setState({ muted: !this.state.muted })
  }

  setPlaybackRate = e => {
    this.setState({ playbackRate: parseFloat(e.target.value) })
  }

  onPlay = () => {
    console.log('onPlay')
    this.setState({ playing: true })
  }

  onPause = () => {
    console.log('onPause')
    this.setState({ playing: false })
  }

  onSeekMouseDown = e => {
    this.setState({ seeking: true })
  }

  onSeekChange = e => {
    this.setState({ played: parseFloat(e.target.value) })
  }

  onSeekMouseUp = e => {
    this.setState({ seeking: false })
    this.player.seekTo(parseFloat(e.target.value))
  }

  onProgress = state => {
    console.log('onProgress', state)
    // We only want to update time slider if we are not currently seeking
    if (state.playedSeconds > 300) {
      this.setState({ playing: false })
      this.props.onFinish()
    }
    if (!this.state.seeking) {
      this.setState(state)
    }
  }

  onEnded = () => {
    console.log('onEnded')
    this.setState({ playing: this.state.loop })
    this.props.onFinish()
  }

  onDuration = (duration) => {
    console.log('onDuration', duration)
    this.setState({ duration })
  }

  renderLoadButton = (url, label) => {
    return (
      <button onClick={() => this.load(url)}>
        {label}
      </button>
    )
  }

  ref = player => {
    this.player = player
  }

  /**
   * rendering
   */

  render () {
    const { url, playing, volume, muted, loop, played, loaded, duration, playbackRate, hidden } = this.state
    const SEPARATOR = ' · '

    const controls = <div className='MusicControls'>
                     <table className={ DEBUG ? '' : 'hide' }><tbody>
                      <tr>
                        <th>Seek</th>
                        <td>
                          <input
                            type='range' min={0} max={1} step='any'
                            value={played}
                            onMouseDown={this.onSeekMouseDown}
                            onChange={this.onSeekChange}
                            onMouseUp={this.onSeekMouseUp}
                          />
                        </td>
                      </tr>
                      <tr>
                        <th>played</th>
                        <td><progress max={1} value={played} /></td>
                      </tr>
                      <tr>
                        <th>URL</th>
                        <td className={!url ? 'faded' : ''}>
                          {(DEBUG ? url : '')}
                        </td>
                      </tr>
                      <tr>
                        <th>played</th>
                        <td>{played.toFixed(3)}</td>
                      </tr>
                      <tr>
                        <th>elapsed</th>
                        <td><Duration seconds={duration * played} /></td>
                      </tr>
                      <tr>
                        <th>remaining</th>
                        <td><Duration seconds={300 - (duration * played)} /></td>
                      </tr>
                    </tbody></table>
                    <table><tbody>
                      <tr className="playBtn">
                        <td>
                          <button onClick={this.playPause}>{playing ? 'Pause' : 'Play'}</button>
                        </td>
                      </tr>
                    </tbody></table>
                  </div>

    if (!hidden) {
      return (
        <div className='App'>
          <section className='section'>
            <p>Please click play when ready.  The app will automatically continue when the song has finished.</p>
            <div className='player-wrapper'>
              <ReactPlayer
                ref={this.ref}
                className='react-player'
                width='0%'
                height='0%'
                url={url}
                playing={playing}
                loop={loop}
                playbackRate={playbackRate}
                volume={volume}
                muted={muted}
                onReady={() => console.log('onReady')}
                onStart={() => console.log('onStart')}
                onPlay={this.onPlay}
                onPause={this.onPause}
                onBuffer={() => console.log('onBuffer')}
                onSeek={e => console.log('onSeek', e)}
                onEnded={this.onEnded}
                onError={e => console.log('onError', e)}
                onProgress={this.onProgress}
                onDuration={this.onDuration}
              />
            </div>
            {controls}
          </section>
        </div>
      )
    }
    else {
      return (<div className="hiddenPlayer"/>)
    }
  }
}

export default MusicPlayer