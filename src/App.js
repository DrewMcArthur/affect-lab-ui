import React, { Component } from 'react'
import request from 'request'

import './App.css';
import MusicPlayer from './MusicPlayer.js'
import { RestartButton, NextRound } from './Buttons.js'

export const DEBUG = false;

class SetupView extends Component {
  state = {
    idInputVal: this.props.inputVal || ''
  }

  submitID() {
    if (this.idInput !== null) {
      this.props.callback(this.state.idInputVal, Math.round(Math.random()), Math.round(Math.random()))
    }
  }
  
  handleChange(e){
    this.setState({idInputVal: parseInt(e.target.value) ? parseInt(e.target.value) : ''})
  }

  render() {
    return (
      <div className="SetupView">
        <h1>Please enter participant ID</h1>
        <p>(last 4 digits of your school ID and last 4 digits of your phone number)</p>
        <input ref={(ref) => this.idInput = ref} 
               value={this.state.idInputVal}
               type='text' 
               pattern="[0-9]*"
               onChange={this.handleChange.bind(this)}
        ></input>
        <button onClick={this.submitID.bind(this)}>Submit</button>
      </div>
    )
  }
}

class RatingButtons extends Component {
  render() {
    const ratings =[1, 2, 3, 4, 5]
    const buttons = ratings.map((i) => <button className="ratingBtn" onClick={() => {this.props.callback(i)}} key={i}>{i}</button>);

    return (
      <ol className="ratingButtons">
        { buttons }
      </ol>
    );
  }
}

class ImageRater extends Component {
  render() {
    const imgurl = DEBUG ? <p>image: {this.props.image}</p> : ''
    return (
      <div className="ImageRater">
        <h1>Image Rater!</h1>
        <img src={this.props.image}/>
        {imgurl}
        <h3>Click a number to rate the image!</h3>
        <h5 style={{marginLeft: '50px'}}>(Dislike Very Much ------------------ Like Very Much)</h5>
        <RatingButtons callback={(i) => {this.props.onFinish(i)}} choices={10}/>
      </div>
    )
  }
}

/**
 * final screen of the computer component, either redirects to the beginning on the first round or 
 * uploads data and provides a link to our demographics survey after the second round
 */
class DoneScreen extends Component {

  /**
   * uploads the app's state to our data spreadsheet for later processing
   * done by sending an email and letting IFTTT do the rest.
   */
  upload() {

    console.log('uploading state: ' + JSON.stringify(this.props.appState))
    request.post("http://18.216.179.122/upload", {
      json: this.props.appState,
      headers: {
        'content-type': 'application/json'
      }
    })

    // // send an email from drewmcarthur1 to trigger@applet.ifttt.com where the body is appState
    // let transporter = nodemailer.createTransport({
    //   service: 'gmail',
    //   auth: GMAILSECRETS
    // });

    // const mailOptions = {
    //   from: 'drewmcarthur1@email.com', // sender address
    //   to: 'trigger@applet.ifttt.com', // list of receivers
    //   subject: 'participant data - ' + this.props.appState.participantId, // Subject line
    //   html: JSON.stringify(this.props.appState) // plain text body
    // };

    // transporter.sendMail(mailOptions, function (err, info) {
    //   if(err)
    //     console.log(err)
    //   else
    //     console.log(info);
    // });

    /*
    request.post("https://script.google.com/macros/s/AKfycbzipOUYfdJM6ODqD3VjRtVASVCv8QIfgqzForFgc6sKFQPzVm-Q/exec", {
                    json: this.props.appState,
                    headers: {
                      'Access-Control-Allow-Origin':'',
                      'Content-Type': 'multipart/form-data'
                    }
                  })
                  */
  }

  /**
   * returns the right element depending on which round of conditioning the user was on. 
   * if first round, we return the NextRound button, which moves us back to the music player again to listen to the opposite audio clip.
   * otherwise, (second/final round) we provide a link to our followup demographics survey.
   */
  getNextButton() {
    const survey = <a target="_blank" href="https://docs.google.com/forms/d/1ejmZN8Or0Jw4CW6R8U86Yi8YOqo9SW5fHIT-JPRiFsg/edit?usp=sharing">Demographics Survey</a>
    const nextRound = <NextRound updateState={this.props.updateState} appState={this.props.appState} />
    return this.props.appState.round == 1 ? survey : nextRound
  }

  render() {
    if (this.props.appState.round == 1) {
      this.upload()
    }
    return (
      <div className="DoneScreen">
        <h1>You rated the image a {this.props.rating}</h1>
        <h3>You're now finished with this portion of the experiment.  Please wait for instructions from the experimenter and inform them of your rating.</h3>
        {this.getNextButton()}
      </div>
    )
  }
}

/**
 * the main content area.  
 * 
 * @props:
 *  updateState - updates the app's state 
 *  appState - the app's state
 */
class Content extends Component {
  showMusic() {
    this.props.updateState({
      setup: false,
      musicPlaying: true
    })
  }

  showImage() {
    this.props.updateState({
      musicPlaying: false,
      imageShowing: true
    })
  }

  /**
   * given a rating, save the state and move to the next view (donescreen)
   * @param int rating 
   */
  gotRating(rating) {
    this.props.updateState({
      imageShowing: false,
      ratingReceived: true,
    })
    this.props.appState.ratings.push(rating)
  }

  render() {
    const { setup, musicPlaying, imageShowing, ratingReceived, songs, images, songChoice, imageChoice, round, ratings } = this.props.appState
    if (DEBUG)
      console.log("song choice: " + this.props.appState.songChoice)

    if (setup) 
    {
      return (
        <SetupView callback={(id, songChoice, imageChoice) => 
          {
            console.log("songChoice,imageChoice was " + songChoice + ", " + imageChoice)
            this.props.updateState(
            {
              setup: false,
              musicPlaying: true,

              songChoice: songChoice,
              imageChoice: imageChoice,
              participantId: id,
            })
          }}
        />
      )
    }

    else if (musicPlaying)
      return (
        <MusicPlayer song={songs[(songChoice + round) % 2]} onFinish={() => {this.showImage()}}/>
      )

    else if (imageShowing)
      return (
        <ImageRater image={images[(imageChoice + round) % 2]} onFinish={(rating) => {this.gotRating(rating)}}/>
      )

    else if (ratingReceived)
      return (
        <DoneScreen appState={this.props.appState} updateState={this.props.updateState} rating={ratings[round]}/>
      )

  }
}


class App extends Component {
  state = {
    // which screen are we on
    setup: true,
    musicPlaying: false,
    imageShowing: false,
    ratingReceived: false,

    // app data
    round: 0, 
    songs: [
      '/media/happy.mp3',
      '/media/neutral.mp3'
    ],
    images: [
      '/media/symbol1.png',
      '/media/symbol2.png'
    ],

    // user data
    participantId: null,
    ratings: [],
  }

  updateState(obj) {
    this.setState(obj)
  }

  render() {
    return (
      <div className="main">
        <Content updateState={this.updateState.bind(this)} appState={this.state} />
        {/* <RestartButton updateState={this.updateState.bind(this)} appState={this.state} /> */}
      </div>
    )
  }
}

export default App;
