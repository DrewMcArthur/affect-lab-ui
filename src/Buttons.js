import React, { Component } from 'react';
import { DEBUG } from './App.js'

export class RestartButton extends Component {
  handleClick() {
    this.props.updateState({
      setup: true,
      musicPlaying: false,
      imageShowing: false,
      ratingReceived: false,
      secondRound: false
    })
  }
  
  render() {
    return (
      <button className="RestartButton" onClick={this.handleClick.bind(this)}>Restart Study</button>
    )
  }
}

export class NextRound extends Component {
  
  /**
   * starts the user on the next round by moving to the music view and 
   * changing state.round to 1 (second round, but 0-indexed)
   */
  handleClick() {
    this.props.updateState({
      setup: false,
      musicPlaying: true,
      imageShowing: false,
      ratingReceived: false,
      round: 1
    })
  }

  render() {
    return (
      <button className="NextButton" onClick={this.handleClick.bind(this)}>Next Round</button>
    )
  }
}